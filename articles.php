<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Articles et commentaires</title>
        <link rel="stylesheet" type="text/css" href="css/joli.css" media="screen"> 
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>

        <!-- ICI FORMULAIRE POUR saisir un nouvel article et afficher les articles déjà écrits -->

    <body>
      <div class=" container bord texte">

          <div class="container">
            <div class="row">
              <h1 class="titre">LA MOUETTE AUX YEUX DE LYNX</h1>
            </div>
          </div>

          <hr class="trait">

          <div class="container pl-5">
            <div class="row">
              <h2>Journal indépendant, articles de nos internautes (publications libres)</h2>
            </div>
          </div><br>

          <div class="container p-5">
            <div class="row">
              <div class="col-md-8">
                <img src="FetesNB.png" alt=""/>
              </div>
              <div class="col-md-4">
                <fieldset>
                    <?php
                      /* Fonctions nécessaires */
                      require 'config.php';
                      require 'functions.php';
                      $connection = GETPDO($config);

                      if(isset($_POST['Envoyer'])){
                        $titre = $_POST['titre'];
                        $article = $_POST['article'];
                        $auteur = $_POST['auteur'];

                        /* Insertion des données dans la table */
                        if (!empty($titre) and !empty($article) and !empty($auteur)) 
                        {
                          $reponse = $connection->prepare("INSERT INTO article (TitreArt,ContenuArt,AuteurArt,DateCreaArt) VALUES (:t,:a,:au,NOW())");
                          /* fonction "prepare" qui sécurise les requetes */
                          $reponse->execute(array('t' => $titre, 'a' => $article, 'au' => $auteur));
                        } 
                        else 
                        {
                          echo "erreur, réessaye petite patate";
                        }
                      }

                      /* Récupération des articles */
                      $sql = "SELECT * FROM article ORDER BY NumArt ASC LIMIT 1";
                      $response = $connection->query($sql);
                      $maxLength = 500;

                      while ($data = $response->fetch()):
                        echo "<strong>" . $data['TitreArt'] . "</strong><br>";
                
                        $contenuCrop = (strlen($data['ContenuArt']) < $maxLength ) ? $data['ContenuArt'] : substr($data['ContenuArt'], 0, $maxLength)."...";
                        echo $contenuCrop . ".<br>";
                
                        echo "Le " . $data['DateCreaArt'] . " par " . $data['AuteurArt'] . ".<br>";
                    ?>

                    <a href="commentaires.php?id=<?php echo $data['NumArt'];?>">Lire l'article</a><br/><br>

                    <?php
                    endwhile;
                    ?>
                </fieldset>
              </div>
            </div>
          </div><br>

          <div class="container pl-5 pr-5">
            <div class="row">
                <fieldset>
                    <?php
                      /* Récupération des articles */
                      $sql = "SELECT * FROM article  WHERE NumArt>'3' ORDER BY NumArt ASC";
                      $response = $connection->query($sql);
                      $maxLength = 1000;
                      
                      while ($data = $response->fetch()):
                        echo "<strong>" . $data['TitreArt'] . "</strong><br>";
                
                        $contenuCrop = (strlen($data['ContenuArt']) < $maxLength ) ? $data['ContenuArt'] : substr($data['ContenuArt'], 0, $maxLength)."...";
                        echo $contenuCrop . ".<br>";
                
                        echo "Le " . $data['DateCreaArt'] . " par " . $data['AuteurArt'] . ".<br>";
                

                    ?>
                      <a href="commentaires.php?id=<?php echo $data['NumArt'];?>">Lire l'article</a><br/><br>
                    <?php
                    endwhile;
                    ?>
                </fieldset>
              </div>
          </div><br>

          <hr class="trait">
          
          <div class="container p-5">
            <div class="row">
              <h2>Participez vous aussi à notre journal libre en écrivant un article</h2>
            </div>
          </div><br>

          <div class="container">
            <div class="row">
              <form action="" method="POST" name="Message">
                <fieldset>
                    <label>Titre : </label><input type="text" name="titre" id="titre"><br>
                    <label>Article : </label><textarea name="article" id="article" rows="5" cols="33"></textarea><br>
                    <label>Auteur : </label><input type="text" name="auteur" id="auteur"><br>
                    <input type="submit" value="Publier" name="Envoyer">
                </fieldset>
              </form>
            </div>
          </div>

      </div>

    </body>
</html>
