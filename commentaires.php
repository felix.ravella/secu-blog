<!-- commentaires : liste des commentaires du  blog  pour chaque  article. 
Tous  les commentaires, quel  que  soit  l’article auquel  ils  se rapportent,  seront stockés dans  la  même table. 
On  pourra  faire  le  tri  facilement  à  l'aide  d'un  champ id_billet  qui  indiquera  pour  chaque  commentaire  le 
numéro de l’article associé. 
Le but de cette page est d'afficer un article, et ses commentaires
Comme nous n'allez pas créer les formulaires d'ajout d’articles et de commentaires dans 
un  premier  temps,  je  vous  conseille  de  remplir  vous -mêmes  les  tables  à  l'aide  de 
phpMyAdmin après les avoir créées. La structure des pages est simple, comme le montre la  figure suivante  : 
Le  visiteur  arrive  d'abord  sur  la  page  articles.php  où  sont  affichés  les  derniers  articles.  S'il  choisit  
d'afficher les commentaires  de l'un d'eux, il charge la page  commentaires.php qui  affichera  l’article 
sélectionné ainsi que tous ses commentaires. IL faudra ensuite envoyer un paramètre à la 
page  commentaires.php pour qu'elle sache afficher l’article avec les commentaires qu’ils lui sont assignés. 
Il sera possible  de revenir à la liste des articles depuis  les commentaires à l'aide  d'un  lien de retour -->


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Articles et commentaires</title>
        <link rel="stylesheet" type="text/css" href="css/joli.css" media="screen"> 
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    </head>



        <!-- ICI FORMULAIRE POUR l'article ET AFFICHAGE DES commentaires -->

    <body>
      <div class=" container bord texte">

      <div class="container">
            <div class="row">
              <h1 class="titre">LA MOUETTE AUX YEUX DE LYNX</h1>
            </div>
          </div>

          <hr class="trait">

          <div class="container pl-5">
            <div class="row">
              <h2>Journal indépendant, articles de nos internautes (publications libres)</h2>
            </div>
          </div><br>

        <!-- Partie qui permet de se connecter à la BDD -->
        <?php
          require "config.php"; /*permet de parametrer la config */
          require 'functions.php';

          /* Déclaration/initialisation des variables du doc */
          $connexion = GETPDO($config);
          $id = $_GET['id'];

        if( isset($_POST['submit']) ) {
          $contenuComm = $_POST['contenuComm'];
          $auteurComm = $_POST['auteurComm'];

          $commInsert = $connexion->prepare("INSERT INTO commentaire(ContenuComm,AuteurComm,DateCreaComm,NumArt) VALUES(:contenu,:auteur,NOW(),:idArt)");
          $params = array('contenu' => $contenuComm, 'auteur' => $auteurComm, 'idArt' => $id);
          $commInsert->execute($params);
        }

          /* Partie qui permet de se connecter à la BDD pour récupérer les articles */
          $resultatArticle = $connexion -> query("SELECT * FROM article WHERE NumArt=$id");
        ?>
        <div class="container">
          <div class="row">
            <fieldset>
              <?php
              /* Affiche les données qui sont dans $reponse (toutes les lignes du tableau?)*/
              while ($ligneDuTableau = $resultatArticle->fetch()) /* cherche et vide les données du tableau */
                {
                  echo "<strong>".$ligneDuTableau['TitreArt']." <br><br> </strong>".$ligneDuTableau['ContenuArt']."<br><br> <strong>".$ligneDuTableau['AuteurArt']." <br> </strong>".$ligneDuTableau['DateCreaArt']."<br/>";
                }
              ?>
            </fieldset>
          </div>
        </div>

        <hr class="trait">

        <?php
          $resultatCommentaire = $connexion -> query("SELECT * FROM commentaire WHERE NumArt=$id");
        ?>
        <div class="container">
          <div class="row margetitre">
            <h4>Commentaires</h4>
          </div>
        </div>

        <div class="container">
          <div class="row">
            <fieldset>
              <?php
              /* Afficher les données qui sont dans $reponse (toutes les lignes du tableau?)*/
              while ($ligneDuTableau = $resultatCommentaire->fetch()) /* cherche et vide les données du tableau */
                {
                  echo $ligneDuTableau['ContenuComm']."<br> <strong>".$ligneDuTableau['AuteurComm']." </strong> <br>".$ligneDuTableau['DateCreaComm']."<br><br/>";
                }
              ?>
            </fieldset>

          </div>
        </div>

        <hr class="trait">

        <div class="container">
          <form method="POST" action="">
            <fieldset>
              <legend>Ajouter un commentaire</legend>

              <label for="login"> Contenu : </label><br>
              <textarea name="contenuComm" cols="33" rows="5"></textarea>
              <br>
              <label for="body"> Auteur: </label>
              <input type="text" name="auteurComm">
              <br>
              <input type="submit" value="Publier" name="submit"> <br> <br>
              <a type="submit" href="articles.php">Retour aux articles</a>
            </fieldset>
          </form>
        </div>

        </br>

      </div>
    </body>

</html>
