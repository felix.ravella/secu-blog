-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 23 déc. 2021 à 15:12
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `blog`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `NumArt` int(11) NOT NULL AUTO_INCREMENT,
  `TitreArt` varchar(255) NOT NULL,
  `ContenuArt` text NOT NULL,
  `AuteurArt` varchar(255) NOT NULL,
  `DateCreaArt` date NOT NULL,
  PRIMARY KEY (`NumArt`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `article`
--

INSERT INTO `article` (`NumArt`, `TitreArt`, `ContenuArt`, `AuteurArt`, `DateCreaArt`) VALUES
(3, 'Bonnes fêtes!', 'Bonjour chers lecteur.e.s,\r\nToute l\'équipre de la rédaction (à savoir Sarah et Félix) vous souhaitent de bonnes fêtes de fin d\'année. \r\nComme toutes et tous, nous souhaitons que l\'année prochaine soit plus simple: moins de Covid et plus de loisirs! Nous vous souhaitons donc une excellente santé pour l\'année à venir (et les suivantes bien sûr), ainsi que pleins de bonnes choses, en fonction de vos envies. Bref: Joyeuses fêtes et bonne année!', 'La rédac\'', '2021-12-22'),
(4, 'Le Nexus VI', 'Embarquez à bord du vaisseau Nexus VI et suivez le Capitaine ainsi que son équipage aux quatre coins de la Galaxie pour votre chronique spécialisée dans la Science-Fiction sous toutes ses formes (Cinéma, Littérature, Jeux Vidéos, Animés etc.) !\r\n\r\nC’est lors de leurs aventures que le Capitaine vous présentera son analyse d’une œuvre, d’un sous-genre ou d’un thème récurrent de la SF.\r\n\r\nCoup de gueule ou coup de cœur, quel que soit son avis, il saura toujours vous l’exposer de façon argumentée et claire (et à coup de Blaster éventuellement).\r\n\r\nC’est en décembre 2014 que le premier épisode de la chaîne Youtube Nexus VI est mis en ligne et, un an plus tard, ce ne sont pas moins de 53 000 personnes qui suivent assidûment la chaîne.\r\n\r\nNexus VI présente des chroniques vidéo sur des œuvres de science-fiction aussi bien cinématographiques que littéraires ou vidéoludiques.\r\n\r\nLa Chaîne a pour ambition d’immerger ses spectateurs dans un monde à part entière et c’est pourquoi les chroniques se déroulent elles-mêmes dans un univers de science-fiction et sont présentées par le Capitaine du vaisseau Nexus VI.\r\n\r\nLes critiques et analyses du Capitaine se font dans le cockpit du vaisseau et chaque épisode comporte un arc narratif avec introduction et conclusion scénarisées.\r\n\r\n \r\n\r\nCe dernier aspect est particulièrement important puisqu\'il a amené Nexus VI a passer un cap, début février 2016, avec le lancement des deux premiers épisodes de la web série : Nexus VI Legends.\r\n\r\nTotalement auto-produite, la série a rencontré un certain succès sur Youtube (82 000 vues pour le premier épisode) et fut relayée par de nombreux vidéastes français influents avec, en premier lieu, Le Fossoyeur de Films qui participe au projet en prêtant ses traits au personnage de Voltaire Wingfall.\r\n\r\n\r\nLa chaîne Nexus VI bénéficie également de moyens techniques professionnels, grâce à l’apport de la société de production Fensch Toast, qui assurent au projet une qualité de production supérieure à la moyenne des productions sur le Youtube français.', 'Sarah', '2021-12-22'),
(5, 'Le Basilic (plante)', 'Le Basilic ou Basilic romain (Ocimum basilicum L.) est une espèce de plantes herbacées thérophytes de la famille des Lamiacées (labiacées, labiées), cultivée comme plante aromatique et condimentaire. La plante est parfois appelée Basilic commun, Basilic aux sauces, Herbe royale1 ou Grand Basilic. Le Basilic commun est largement employé dans la cuisine italienne, d\'autres variétés de basilic sont répandues dans certaines cuisines asiatiques : Taïwan, Thaïlande, Viêt Nam, Cambodge et Laos. Cette plante a eu plusieurs synonymes scientifiques dont Ocimum basilicum var. glabratum Benth, Ocimum basilicum var. majus Benth.\r\n\r\nLe mot basilic peut parfois désigner d\'autres espèces appartenant au genre Ocimum dont le basilic citron (Ocimum canum L .), le basilic de Ceylan (Ocimum gratissimum) ou encore le basilic sacré (Ocimum sanctum L .) \r\n\r\nDans la gastronomie, les feuilles de basilic sont utilisées comme herbe aromatique. Elles s\'utilisent de préférence crues car leur arôme s\'atténue à la cuisson. Elles accompagnent les crudités (salades, tomates courgettes), les pâtes, les coquillages et les poissons, les œufs brouillés, le poulet, le lapin, le canard, les sauces (vinaigrettes, au citron, à l\'huile d\'olive).\r\n\r\n    La soupe au pistou est la recette de soupe traditionnelle de la Provence. C\'est une soupe que l\'on déguste principalement l\'été. Pistou provient du latin pistare qui signifie « piler », car il est indispensable d\'écraser les feuilles de basilic au mortier pour bien les incorporer à la préparation. Le mot pistou désigne donc la pommade qui accompagne cette soupe et non le plant de basilic.\r\n    Pesto (Ligurie) : recette du nord-ouest de l\'Italie. Pilé avec de l\'huile d\'olive, du pecorino (éventuellement du parmesan), des pignons et de l\'ail, il donne une sauce onctueuse pour accompagner les pâtes.\r\n    Pistou (sud de la France) : c\'est une recette proche du pesto italien, mais qui se prépare sans les pignons, pour accompagner les soupes d\'été de légumes et de haricots blancs, les pâtes, les beignets de courgette ou d\'aubergine.\r\n    Spaghetti aux herbes.\r\n    Velouté de basilic au parmesan.\r\n\r\nSous forme d\'extraits (huile essentielle en général), il entre dans les préparations d\'additifs, parfums et arômes, de produits pharmaceutiques et cosmétiques ou encore de liqueurs ou d\'aliments10, la plante fraîche distillée donnant une essence parfumée par l\'eucalyptol et l\'eugénol11,12 qu\'elle contient.\r\nGraines de basilic.\r\n\r\nOn utilise aussi ses graines que l\'on fait tremper dans l\'eau pour libérer son enveloppe gélatineuse, texture appréciée dans des desserts comme le falooda, le sharbat-e-rihan, le hột é ou le ais kacang. ', 'Félix', '2021-12-22');

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `IDComm` int(11) NOT NULL AUTO_INCREMENT,
  `ContenuComm` text NOT NULL,
  `AuteurComm` varchar(255) NOT NULL,
  `DateCreaComm` date NOT NULL,
  `NumArt` int(11) NOT NULL,
  PRIMARY KEY (`IDComm`),
  KEY `CommToArt` (`NumArt`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commentaire`
--

INSERT INTO `commentaire` (`IDComm`, `ContenuComm`, `AuteurComm`, `DateCreaComm`, `NumArt`) VALUES
(3, 'Waouh, ça pose ses burnes niveau production, je crois que j\'ai encore jamais rien vu de tel sur YouTube. Un grand bravo à toute l\'équipe !', 'AlterHis', '2021-12-23', 4),
(4, 'Génial, une telle production sur Youtube c\'est dingue . \r\nMerci beaucoup, hâte de voir la suite .\r\nVous méritez 10 x plus d\'abonnés !!!!', 'edgar besson', '2021-12-23', 4),
(5, 'Excellent plante, mais ne sert pas que dans la cuisine!', 'Sarah', '2021-12-23', 5),
(6, 'Merci et bonnes fêtes à vous! ', 'Jackjack', '2021-12-23', 3);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commentaire`
--
ALTER TABLE `commentaire`
  ADD CONSTRAINT `CommToArt` FOREIGN KEY (`NumArt`) REFERENCES `article` (`NumArt`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
