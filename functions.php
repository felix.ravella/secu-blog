<?php

  function getPDO($config) {
    $host = $config['host'];
    $dbname = $config['dbname'];
    $login = $config['login'];
    $password = $config['password'];

    try {
      $pdo = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $login, $password);
      return $pdo;
    } catch (Exception $e) {
      die('Erreur: '. $e->getMessage());
    }
  }

?>

